pragma solidity ^0.4.23;

import "./ILaboCryptoService.sol";

contract LaboCryptoServiceV2 is ILaboCryptoService {
    uint256 constant secret = 333333;

    function encrypt(uint256 _message) public view returns (uint256) {
        return _message + secret;
    }

    function decrypt(uint256 _message) public view returns (uint256) {
        return _message - secret;
    }
}