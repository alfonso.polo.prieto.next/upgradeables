pragma solidity ^0.4.23;

contract ILaboCryptoService {
    function encrypt(uint256 _message) public view returns (uint256);
    function decrypt(uint256 _message) public view returns (uint256);
}