primeras pruebas sobre como hacer upgradeables los smartcontracts
===

en esta prueba, nos hemos centrado en la logica de negocio. En nuestro contrato hacemos facilmente intercambiable/creable  la implementacion de la interface ILaboCryptoService con los metodos **setupLcService** y **setupEStorage** del smartContract oficial aka **Labo01Contract05**

se puede hacer deploy inicialmente las implementaciones **LaboCryptoServiceV0**, **LaboCryptoServiceV1**, **LaboCryptoServiceV2** y **LaboCryptoServiceV3** y luego setear la address en el deploy de **Labo01Contract04** o a discrepcion cambiar usando el **Labo01Contract05.setupLcService**

para el storage, hacemos uso de la implementacion **EternalStorageV2** seteando con **Labo01Contract05.setupEStorage**, se puede jugar con varios despliegues de esta implementacion y ver como se alteran las fuentes de datos

para hacer funcionar el smartcontract son necesarios 3 despliegues y ejecutar 2 funciones del smartcontract, a saber ... 
* 1 deploy para *ILaboCryptoService*, por ejemplo desplegar ... **LaboCryptoServiceV1**
* 1 deploy para *IEternalStorage* ... desplegando **EternalStorageV2**
* 1 deploy de **Labo01Contract05**, en este momento el contrato no puede funcionar necesita que le indiquemos que *ILaboCryptoService* y *IEternalStorage*
* ejecucion de *Labo01Contract05.setupLcService* con la direccion de el/un *ILaboCryptoService* ya desplegado
* ejecucion de *Labo01Contract05.setupEStorage* con la direccion de el/un *IEternalStorage* ya desplegado

es una POC ... en la que podemos "imaginarnos" que se pretende guardar de forma encriptada un valor. La logica de encriptacon incialmente en la V0 fallaba, despues se implementa una V1 pero se ve que es muy debil, en la V2 se cambia el secreto para endurecer, pero sigue sin convencer ... hasta encontrar una V3 que ya juega con algebra modular y endurece definitivamente nuestro desarrollo

el metodo **Labo01Contract04.storedMessageRaw** es muy util para ver/jugar con el sistema

