pragma solidity ^0.4.23;

import "./ILaboCryptoService.sol";

contract LaboCryptoServiceV3 is ILaboCryptoService {
    uint256 constant secret = 999983;
    uint256 constant max = 222;

    function encrypt(uint256 _message) public view returns (uint256) {
        return (secret * randNumber(max)) + _message;
    }

    function decrypt(uint256 _message) public view returns (uint256) {
        return _message % secret;
    }

    uint nonce = 8888;
    function randNumber(uint _max) internal returns(uint) {
        return uint(keccak256(block.timestamp, msg.sender, nonce++)) % _max;
    }
}