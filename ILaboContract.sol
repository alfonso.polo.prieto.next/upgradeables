pragma solidity ^0.4.23;

contract ILaboContract {
    function putMsg(uint256 _message) public;
    function lastMsg() public view returns (uint256);
}