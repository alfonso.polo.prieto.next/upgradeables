pragma solidity ^0.4.23;

import "./ILaboContract.sol";
import "./ILaboCryptoService.sol";
import "./IEternalStorage.sol";

contract Labo01Contract05 is ILaboContract {
    /* switchable BUSSINES_LOGIC */
    ILaboCryptoService lcService;
    function setupLcService(address _lcServiceAddress) public {
        lcService = ILaboCryptoService(_lcServiceAddress);
    }

    /* flexible STORAGE */
    IEternalStorage eStorage;
    function setupEStorage(address _storage) public {
        eStorage = IEternalStorage(_storage);
        eStorage.setUint( keccak256("storedMessage"), 9999 );
    }
    
    /* ILaboContract INTERFACE , minimal use of BUSSINES_LOGIC + STORAGE */
    function putMsg(uint256 _message) public {
        uint256 val = lcService.encrypt(_message);
        eStorage.setUint( keccak256("storedMessage"), val );
    }
    function lastMsg() public view returns (uint256) {
        uint256 val = eStorage.getUint( keccak256("storedMessage") );
        return lcService.decrypt(val);
    }






    /* ONLY utility */
    function storedMessageRaw() public view returns (uint256) {
        return eStorage.getUint( keccak256("storedMessage") );
    }
}