smartcontracts para 2 mundos ... equilibrio
===
es importante conocer los 2 sabores extremos en los que nos podemos mover con un smartcontract.

Por un lado tenemos la implementacion publica+descentralizada+masiva ... en la que hay que aportar un contrato simple de explicar, de leer su codigo, pasarle tests/audiotarias/verificacionFormal para confirmar que no tiene una posible debilidad a explotar y que ninguna de las partes tiene el poder de cabiarlo o ponerse en situacion dominante que quite el sentido principal de estos smartcontracts

Esta implementacion no es lo mismo a otro posible uso ... en el que se ofrece un servicio mas clasico en el cual se busca poder cambiar el smartcontract por parte de un proveedor. Esta flexibilidad produce mas complejidad y mas poder a una de las partes, es un escenario mas clasico y contrario al anterior pero susceptible de ser requerido/necesario para algunos desarrollos.



smartcontracts upgradeables via interface+implementacion Y ethernalStorage
===
en la POC actual hemos afrontado la solucion al problema de la siguiente forma ... las pautas que proponemos son ...

* segregar logica solo de ejecucion de logica solo de acceso al almacenamiento, esto requiere ser lo suficientemente estricto con patrones de dise~o clasicos, sera interesante manejar un modelo para procesar y almacenar ... y como nexo posible estos patrones de dise~o (patrones factoria, estado ...)
* utilizar en logica de ejecucion practicas interface+implemetacionSinStorage
* para la logica de almacenamiento hemos optado por el almacenamiento flexible/reutilizable/deshechable/migrable, esto es posible utilizando una estrategia usando el denominado ethernalStorage
* en una 2a aproximacion las implementaciones interface+implemetacionSinStorage deberian poder pasarse a a libreria (ya que a no tener storage facilita el cambio) si esto fuese necesario

con estas pautas podemos soportar futuros requerimientos de flexibilidad para smartcontracts



smartcontracts upgradeables via otras estrategias
===
en ... https://github.com/zeppelinos/labs/ tenemos otras estrategias ademas de la que hemos utilizado, el proyecto "zeppelin OS" tiene como objetivo cubrir estos aspectos y esta trabajando para proveer liberias/sdk/utilidades que podrian dar solucion mas simple a estas cuestiones



doc, referencias ... utiles
===
aqui dejo algunos links, hay muchos posts sobre el tema ... aqui solo estan un pocos que pueden servir de base

articulos interesantes
---
https://blog.zeppelinos.org/smart-contract-upgradeability-using-eternal-storage/
https://medium.com/rocket-pool/upgradable-solidity-contract-design-54789205276d
https://blog.colony.io/writing-upgradeable-contracts-in-solidity-6743f0eecc88

eternal storage  ,,, ERC930
---
https://github.com/ethereum/EIPs/issues/930
https://github.com/AugustoL/zeppelin-solidity/blob/eternal-storage-example/contracts/examples/ESToken.sol
https://github.com/AugustoL/zeppelin-solidity/blob/eternal-storage-example/contracts/storage/EternalStorage.sol

uso de zos-lib ,,, zos=zeppelinOS ,,, zeppelinOS trabajando sobre estos asuntos
---
https://github.com/zeppelinos/labs
https://github.com/zeppelinos/zos-lib
https://zeppelinos.org/
https://github.com/zeppelinos/zos-lib/tree/2cf0aad3e4fb4d97e694b74fa73e9de680214657/examples/single
https://blog.zeppelinos.org/proxy-patterns/

