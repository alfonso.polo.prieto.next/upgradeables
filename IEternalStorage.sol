pragma solidity ^0.4.23;

contract IEternalStorage{
    function setBoolean(bytes32 h, bool v) public;
    function setInt(bytes32 h, int v) public;
    function setUint(bytes32 h, uint256 v) public;
    function setAddress(bytes32 h, address v) public;
    function setString(bytes32 h, string v) public;
    function setBytes(bytes32 h, bytes v) public;
    
    function getBoolean(bytes32 h) public view returns (bool);
    function getInt(bytes32 h) public view returns (int);
    function getUint(bytes32 h) public view returns (uint256);
    function getAddress(bytes32 h) public view returns (address);
    function getString(bytes32 h) public view returns (string);
    function getBytes(bytes32 h) public view returns (bytes);
}
